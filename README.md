# Web decision support platform

MEAN application

# Config
To use in local, modify auth.service.ts routes to `/users/register` for example.
If deploying the app, modify these routes to `http://linktotheapi/users/register`. 

# Run

If you run only the back-end, the app will be accessible at http://localhost:8080/, but only the code that was previously build using `ng build` in angular-src folder. For live preview and dev workflow, launch the server and the front-end.

## running back-end node.js app
run either one of these commands at the root of the project.

`nodemon // this will allow live refresh`

`node app.js // no live refresh`


## running the front-end angular
run this command at the angular-src root.

`ng serve // this will build newly modified angular components when files are saved`

## building the application
go to the angular-src folder and run this command. It will build the components of the front-end and the back-end in the public folder, being accesible on localhost:8080 when running the node server.
`ng build`
