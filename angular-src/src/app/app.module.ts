import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './material/material.module';
import { SidenavModule } from './components/sidenav/sidenav.module';
import { AgmCoreModule } from '@agm/core';
import { CountdownModule } from 'ngx-countdown';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RouteComponent } from './components/route/route.component';

import { ValidateService } from './services/validate.service';
import { HarborsService } from './services/harbors.service';
import { SocketService } from './services/socket.service';
import { ShortestpathService } from './services/shortestpath.service';
import { FlashMessagesModule } from 'angular2-flash-messages';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { LiveComponent } from './components/live/live.component';
import { ChatComponent } from './components/chat/chat.component';
import { ChatService } from './components/chat/chat.service';

const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'register', component:RegisterComponent},
  {path:'login', component:LoginComponent},
  {path:'dashboard', component:DashboardComponent, canActivate:[AuthGuard]},
  {path:'profile', component:ProfileComponent, canActivate:[AuthGuard]},
  {path:'route', component:RouteComponent, canActivate:[AuthGuard]},
  {path:'live', component:LiveComponent, canActivate:[AuthGuard]},
  {path:'chat', component:ChatComponent, canActivate:[AuthGuard]}
]


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    RouteComponent,
    LiveComponent,
    ChatComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCmMwsGADlFBP11LIBWrEZTRkNrkGU7H4w'
    }),
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    AngularMaterialModule,
    SidenavModule,
    ReactiveFormsModule,
    CountdownModule
  ],
  providers: [
    ValidateService,
    AuthService,
    AuthGuard,
    HarborsService,
    SocketService,
    ShortestpathService,
    ChatService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
