import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class HarborsService {

  constructor(
    private http: Http
  ) { }

  getAllHarbors() {
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      return this.http.get('http://localhost:8090/harbors/all', {headers: headers})
        .map(res => res.json());
  }

}
