import { TestBed, inject } from '@angular/core/testing';

import { HarborsService } from './harbors.service';

describe('HarborsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HarborsService]
    });
  });

  it('should be created', inject([HarborsService], (service: HarborsService) => {
    expect(service).toBeTruthy();
  }));
});
