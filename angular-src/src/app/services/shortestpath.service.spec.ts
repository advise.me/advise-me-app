import { TestBed, inject } from '@angular/core/testing';

import { ShortestpathService } from './shortestpath.service';

describe('ShortestpathService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShortestpathService]
    });
  });

  it('should be created', inject([ShortestpathService], (service: ShortestpathService) => {
    expect(service).toBeTruthy();
  }));
});
