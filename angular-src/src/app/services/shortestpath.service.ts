import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

@Injectable()
export class ShortestpathService {

  constructor(
    private http: Http
  ) { }

  getShortestPath(lat1, long1, lat2, long2) {
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      return this.http.get('http://localhost:5000/'+ lat1 + '/' + long1 + '/' + lat2 + '/' + long2, {headers: headers})
        .map(res => res.json());
  }

}
