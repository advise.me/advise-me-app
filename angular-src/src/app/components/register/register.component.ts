import { Component, OnInit } from '@angular/core';
import { ValidateService } from '../../services/validate.service';
import { AuthService } from '../../services/auth.service';
import { FlashMessagesService } from 'angular2-flash-messages'
import { Router } from '@angular/router';
import { Http, Headers } from '@angular/http';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  firstname: String;
  lastname: String;
  function: String;
  organism: String;
  username: String;
  email: String;
  password: String;
  functions;

  constructor(
    private validateService: ValidateService,
    private flashMessage: FlashMessagesService,
    private authService: AuthService,
    private router: Router,
    private http: Http
  ) { }

  ngOnInit() {
    this.getFunctions().subscribe(functions => {
      this.functions = functions.functions;
    },
    err => {
      console.log(err);
      return false;
    });
  }

  getFunctions() {
      let headers = new Headers();
      headers.append('Accept', 'application/json');
      return this.http.get('http://localhost:8090/functions/all', {headers: headers})
        .map(res => res.json());
  }

  onRegisterSubmit(){
    const user = {
      firstname: this.firstname,
      lastname: this.lastname,
      username: this.username,
      function: this.function,
      organism: this.organism,
      email: this.email,
      password: this.password
    }

    // Required fields
    if(!this.validateService.validateRegister(user)){
      this.flashMessage.show('Please fill in all fields', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    if(!this.validateService.validateEmail(user.email)){
      this.flashMessage.show('Please enter a valid email', { cssClass: 'alert-danger', timeout: 3000 });
      return false;
    }

    // Register user
    this.authService.registerUser(user).subscribe(data => {
      if(data.success){
        window.scrollTo(0, 0);
        this.flashMessage.show('You are now registered and can log in', { cssClass: 'alert-success', timeout: 3000 });
        this.router.navigate(['/login']);
      } else {
        window.scrollTo(0, 0);
        this.flashMessage.show('Something went wrong : ' + data.msg, { cssClass: 'alert-danger', timeout: 3000 });
        this.router.navigate(['/register']);
      }
    });
  }
}
