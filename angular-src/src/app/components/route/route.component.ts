import { Component, ElementRef, NgModule, NgZone, OnInit, ViewChild, Inject } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { HarborsService } from '../../services/harbors.service';
import { ShortestpathService } from '../../services/shortestpath.service';
import { FlashMessagesService } from 'angular2-flash-messages';


import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';
import {startWith} from 'rxjs/operators/startWith';
import {map} from 'rxjs/operators/map';

export class Harbor {
  constructor(
    public name: string,
    public country: string,
    public latitude: Number,
    public longitude: Number
  ) { }
}

// just an interface for type safety.
interface marker {
lat: number;
lng: number;
city: string;
country: string;
label?: string;
draggable: boolean;
}

@Component({
  selector: 'app-route',
  templateUrl: './route.component.html',
  styleUrls: ['./route.component.css']
})

export class RouteComponent implements OnInit {
  latitude: number;
  longitude: number;
  zoom: number;

  originCtrl: FormControl;
  destinationCtrl: FormControl;

  filteredHarborsOrigin: Observable<any[]>;
  filteredHarborsDestination: Observable<any[]>;
  harbors: Harbor[]= [];

  nbCoords;
  markers: marker[];
  path;
  pathLoading: boolean = false;

  clickedMarker(label: string, index: number) {
    console.log(`clicked the marker: ${label || index}`)
  }

  constructor(
    private authService: AuthService,
    private harborsService: HarborsService,
    private shortestpathService: ShortestpathService,
    private flashMessage: FlashMessagesService
  ) {
    this.originCtrl = new FormControl();
    this.destinationCtrl = new FormControl();

    this.filteredHarborsOrigin = this.originCtrl.valueChanges
      .pipe(
        startWith<string | Harbor>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(harbor => harbor ? this.filterHarbors(harbor) : this.harbors.slice())
      );
    this.filteredHarborsDestination = this.destinationCtrl.valueChanges
      .pipe(
        startWith<string | Harbor>(''),
        map(value => typeof value === 'string' ? value : value.name),
        map(harbor => harbor ? this.filterHarbors(harbor) : this.harbors.slice())
      );
  }

  ngOnInit() {
    this.zoom = 2;

    this.harborsService.getAllHarbors().subscribe(harbors => {
      this.harbors = harbors.harbors;
    },
    err => {
      console.log(err);
      return false;
    });
  }

  initMarkersOnPath(){
    this.nbCoords = this.path.features[0].geometry.coordinates.length;
    this.latitude = this.path.features[0].geometry.coordinates[(this.nbCoords/2)-1][1];
    this.longitude = this.path.features[0].geometry.coordinates[(this.nbCoords/2)-1][0];
    this.markers = [
      {
        lat: this.path.features[0].geometry.coordinates[0][1],
        lng: this.path.features[0].geometry.coordinates[0][0],
        city: this.originCtrl.value.name,
        country: this.originCtrl.value.country,
        label: 'A',
        draggable: false
      },
      {
        lat: this.path.features[0].geometry.coordinates[this.nbCoords - 1][1],
        lng: this.path.features[0].geometry.coordinates[this.nbCoords - 1][0],
        city: this.destinationCtrl.value.name,
        country: this.destinationCtrl.value.country,
        label: 'B',
        draggable: false
      }
    ];
  }

  filterHarbors(name: string): Harbor[] {
  return this.harbors.filter(harbor =>
    harbor.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  onFindItinerary(){
    this.pathLoading = true;
    var origin = this.originCtrl.value;
    var dest = this.destinationCtrl.value;
    this.shortestpathService.getShortestPath(origin.latitude, origin.longitude, dest.latitude, dest.longitude).subscribe(path => {
      this.pathLoading = false;
      this.path = path;
      // this.initMarkersOnPath();
    },
    err => {
      console.log(err);
      this.pathLoading = false;
      this.flashMessage.show("There has been an error while loading this itinerary", {
        cssClass: 'alert-danger',
        tiemout: 10000});
      window.scrollTo(0, 0);
      return false;
    });
  }

  displayHarbor(harbor?: Harbor){
    return harbor ? harbor.name : "";
  }

}
