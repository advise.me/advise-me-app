import { Component, OnInit, AfterViewChecked, ElementRef, ViewChild } from '@angular/core';
import { ChatService } from './chat.service';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import * as io from "socket.io-client";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, AfterViewChecked {
  @ViewChild('scrollMe') private myScrollContainer: ElementRef;

  convs=[];
  activeConv:any;
  messages = [];
  user:any;
  users:any;
  otherUser: string;
  msgData = { expeditor: '', recipient: '', message: '' };
  textToSend:string;
  socket = io('http://localhost:4000');
  listUsers=false;

  constructor(
    private chatService: ChatService,
    private router: Router,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.getUser().subscribe(usr => {
      this.getMyConversations(usr.username);
      this.msgData = { expeditor: usr.username, recipient: '', message: '' }
      this.scrollToBottom();
    });
    this.authService.getAllUsers().subscribe(usrs => {
      this.users = usrs.users;
    });
    this.initSocket();
  }

  initSocket(){
    this.socket.on('new-message', function (data) {
      var conv = data.message.msg;
      if(conv.user1 == this.user.username || conv.user2 == this.user.username) {
        var index = this.getConvIndexByUsers(conv.user1, conv.user2);
        if(index != null) {
          this.convs[index].messages.push(conv.messages[conv.messages.length - 1].content);
          if(this.activeConv != undefined && this.activeConv == this.convs[index]) {
            this.messages.push(conv.messages[conv.messages.length - 1]);
          }
        } else {
          this.convs.push(conv);
        }

      }
      this.scrollToBottom();
    }.bind(this));
  }

  getOtherUser(usr1, usr2) {
    if(usr1 == this.user.username) {
      return this.getUserByUsername(usr2);
    } else {
      return this.getUserByUsername(usr1);
    }
  }

  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
    } catch(err) { }
  }

  getUser() {
    return this.authService.getProfile().map(profile => {
      this.user = profile.user;
      return this.user;
    },
    err => {
      console.log(err);
      return false;
    });
  }

  getMyConversations(username) {
    this.chatService.getConversationsByUsername(username).subscribe((res) => {
      if(res.success && res.msg.length != 0) {
        this.convs = res.msg;
        this.activeConv = this.convs[this.convs.length - 1];
        this.otherUser = this.getOtherUser(this.activeConv.user1,this.activeConv.user2).username;
        for(var i = 0; i < this.activeConv.messages.length;i++){
          this.messages.push(this.activeConv.messages[i]);
        }
      }
    }, (err) => {
      console.log(err);
    });
  }

  addConversation() {
    this.listUsers = !this.listUsers;
  }

  changeConversation(newUser) {
    this.otherUser = newUser;
    this.msgData = { expeditor: this.user.username, recipient: this.otherUser, message: '' };
    var index = this.getConvIndexByUsers(this.user.username, this.otherUser);
    this.chatService.getAConversationByUsername(this.msgData).subscribe((res) => {
      this.messages = [];
      if(res.success && res.msg) { // Messaging API returns a conversation
        this.activeConv = res.msg;
        if(index != null) { //Conversation also exists in local
          this.convs.splice(index,1);
          this.convs.push(this.activeConv);
        }
        for(var i = 0; i < this.activeConv.messages.length;i++){
          this.messages.push(this.activeConv.messages[i]);
        }
      } else { // Conversation not existing in messaging API
        if(index != null) { // Conversation already exists in local
        this.activeConv = this.convs[index];
        for(var i = 0; i < this.activeConv.messages.length;i++){
          this.messages.push(this.activeConv.messages[i]);
        }
      } else { // Not existing at all
        var newConv = {
        _id: '',
        user1: this.user.username,
        user2: this.otherUser,
        messages: []
      };
      this.activeConv = newConv;
      this.convs.push(newConv);
    }
  }
}, (err) => {
  console.log(err);
});
}

getConvIndexByUsers(user1, user2) {
  var res = null;
  for(var i = 0; i < this.convs.length;i++){
    if((this.convs[i].user1 == user1 && this.convs[i].user2 == user2) ||
    (this.convs[i].user1 == user2 && this.convs[i].user2 == user1)) {
      res = i;
    }
  }
  return res;
}

sendMessage() {
  this.msgData = {expeditor: this.user.username, recipient: this.otherUser, message: this.textToSend}
  this.chatService.saveMessage(this.msgData).subscribe((result) => {
    this.socket.emit('save-message', result);
  }, (err) => {
    console.log(err);
  });
  this.textToSend = "";
}

getUserByUsername(username) {
  var userFound;
  for(var i = 0; i < this.users.length;i++){
    if (this.users[i].username == username) {
      userFound = this.users[i];
    }
  }
  return userFound;
}

}
