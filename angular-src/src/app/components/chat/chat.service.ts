import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class ChatService {

  constructor(private http: Http) { }

  getConversationsByUsername(username) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:8091/conversations/' + username, {headers: headers})
        .map(res => res.json());
  }

  getAConversationByUsername(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:8091/conversations/', data, {headers: headers})
          .map(res => res.json())
  }

  saveMessage(data) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:8091/conversations/', data, {headers: headers})
        .map(res => res.json());
  }

}
