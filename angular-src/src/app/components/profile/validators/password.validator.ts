import { AbstractControl } from '@angular/forms';

export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
       let password = AC.get('password').value;
       let passwordConfirm = AC.get('confirmPassword').value;
        if(password != passwordConfirm) {
            console.log('false');
            AC.get('confirmPassword').setErrors( {matchPassword: true} )
        } else {
            console.log('true');
            return null
        }
    }
}
