import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { PasswordValidation } from './validators/password.validator';
import { FormControl, ReactiveFormsModule, FormGroupDirective, FormGroup, FormBuilder, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FlashMessagesService } from 'angular2-flash-messages';

export class ParentErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = !!(form && form.submitted);
        const controlTouched = !!(control && (control.dirty || control.touched));
        const controlInvalid = !!(control && control.invalid);
        const parentInvalid = !!(control && control.parent && control.parent.invalid && (control.parent.dirty || control.parent.touched));

        return (controlTouched && (controlInvalid || parentInvalid));
    }
}


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  user: Object;
  passwordForm: FormGroup;
  parentErrorStateMatcher = new ParentErrorStateMatcher();

  get passwords() { return this.passwordForm.get('passwords'); }
  get oldPassword() { return this.passwordForm.get('passwords.oldPassword'); }
  get password() { return this.passwordForm.get('passwords.password'); }
  get confirmPassword() { return this.passwordForm.get('passwords.confirmPassword'); }

  constructor(
    private router: Router,
    private authService: AuthService,
    private fb: FormBuilder,
    private flashMessage: FlashMessagesService
  ) {
    this.passwordForm = fb.group({
            passwords: fb.group({
                oldPassword: ['', [
                    Validators.required
                ]],
                password: ['', [
                    Validators.required
                ]],
                confirmPassword: ['', [
                    Validators.required,
                    PasswordValidation
                ]]
            },
                {
                    validator: PasswordValidation.MatchPassword
                }),
        });
  }

  ngOnInit() {
    this.loadUser();
  }

  onSaveSubmit() {
    this.authService.updateUser(this.user).subscribe(data => {
      if(data.success){
        window.scrollTo(0, 0);
        this.flashMessage.show("Your informations have been saved!", {
          cssClass: 'alert-success',
          timeout: 5000 });
      } else {
        window.scrollTo(0, 0);
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000 });
      }
    });
  }

  onReset() {
    window.scrollTo(0, 0);
    this.loadUser();
  }

  onChangePassword() {
    let oldp = this.passwords.value.oldPassword;
    let newp = this.passwords.value.password;

    this.authService.changePassword({oldPassword: oldp, password: newp}).subscribe(data => {
      if(data.success){
        window.scrollTo(0, 0);
        this.flashMessage.show("Your password has been changed!", {
          cssClass: 'alert-success',
          timeout: 5000 });
      } else {
        window.scrollTo(0, 0);
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger',
          timeout: 5000 });
      }
    });
    this.loadUser();
  }

  loadUser() {
    this.authService.getProfile().subscribe(profile => {
      this.user = profile.user;
    },
    err => {
      console.log(err);
      return false;
    });
  }


}
