import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatSidenav } from "@angular/material";


@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent implements OnInit {
  @Input()
  sidenav: MatSidenav;

  constructor(
        private router: Router
  ) { }

  ngOnInit() {
  }

  handleClickMenu(event) {
    if (this.sidenav.mode === 'over')
      this.sidenav.close();
  }
}
