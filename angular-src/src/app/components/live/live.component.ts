import { Component, OnInit, ViewChild, ViewEncapsulation, OnDestroy } from '@angular/core';
import { CountdownModule } from 'ngx-countdown';
import { CountdownComponent } from 'ngx-countdown';
import { SocketService } from '../../services/socket.service';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LiveComponent implements OnInit, OnDestroy {
  latitude: Number = 53.4;
  longitude: Number = 2.5;
  zoom: Number = 4;
  config = {
    leftTime: 5
  }
  active : boolean = true;

  messages = [];
  connection;
  message;

  constructor(
    private socketService: SocketService
  ) { }

  @ViewChild('cd1') counter: CountdownComponent;
  onFinished(){
    this.counter.restart();
  }

  sendMessage(){
    this.socketService.sendMessage(this.message);
    this.message = '';
  }

  ngOnInit() {
    this.connection = this.socketService.getMessages().subscribe(message => {
      this.messages.push(message);
    });
  }

  ngOnDestroy() {
    this.connection.unsubscribe();
  }
}
